#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "network.h"
#include "crossing.h"

#define TICK_DELAY_MS	100
#define TAG 			"Main"

void app_main() {
	uint32_t tickCount = 0;

    nvs_flash_init();
    tcpip_adapter_init();

    printf("Starting...\n");

    // Init Event System
    esp_event_loop_create_default();

    // Prepare Wifi & MQTT
    network_init();
    // Prepare software driver for corssing detection
    {
    	uint8_t vl53l1_addresses[1] = { 0x52 }; // Default I2C address of VL53L1. Not that matters as we mock it
    	uint16_t sensorHeights[1] = { 250 }; // Height of the over-hanging sensor in centimeters
    	crossing_init(vl53l1_addresses, sensorHeights, 1);
    }

    // Actually start everything
    network_start();

    printf("Ready\n");
    while (true) {
    	vTaskDelay(TICK_DELAY_MS / portTICK_PERIOD_MS);
    	tickCount += TICK_DELAY_MS;

    	network_run();
    	crossing_run();

    	if (network_isConnected()) {
    		if (tickCount >= (CONFIG_CLOUD_UPDATE_DELAY*60L*1000L)) {
    			tickCount = 0;

    			uint16_t crossingNumber = crossing_getCount(true);
    			ESP_LOGI(TAG, "Crossing from sensors : %d\n", crossingNumber);

    			if (network_transmit(crossingNumber)) {
    				ESP_LOGI(TAG, "Update sent to Cloud\n");
    			} else {
    				ESP_LOGE(TAG, "Error during Cloud transmission\n");
    			}
    		}
    	}
    }
}

//TODO Missing for time reasons
/*
 * Proper error handling (wifi connection error, mqtt connection error)
 * Low-power sleep/wakeup
 * On-board testing plan
 * Improve crossing algorithm (use time de-asserting on crossing detection to avoid noise spikes from sensor ranging)
 * Better use of the RTOS for timely tasks
 */

