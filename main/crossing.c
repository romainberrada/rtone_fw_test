#include "crossing.h"
#include "vl53l1.h"
#include "sdkconfig.h"
#include "esp_log.h"

#define TAG	"Crossing"

static struct VL53L1_CrossingHandle {
	struct VL53L1_Handle hwHandle;
	uint16_t floorDistance_cm;

	int16_t lastHeight_cm;
	uint16_t count;

} _handles[CONFIG_SENSOR_ARRAY_MAX_LENGTH];
static uint8_t _handlesLength;


void crossing_init(uint8_t* i2cArray, uint16_t* floorDistances, int length) {
	for (int i=0; i<length; i++) {
		vl53l1_init(&_handles[i].hwHandle, i2cArray[i]);
		_handles[i].floorDistance_cm = floorDistances[i];
		_handles[i].count = 0;
	}
	_handlesLength = length;
}


void crossing_run() {
	// Check distance from each sensor from the array
	for (int i=0; i<_handlesLength; i++) {

		// run hardware driver
		if (vl53l1_run(&_handles[i].hwHandle)) { // if new range
			int16_t newRange = vl53l1_getLastRange(&_handles[i].hwHandle);

			if (newRange != -1) { // Should never occur, but just in case
				// Compute the height of the object from the ground
				uint16_t objectHeight_cm = _handles[i].floorDistance_cm - newRange/10;

				// Only count a crossing if we get a rising edge in height to avoid (a bit) duplicate counts
				if (objectHeight_cm >= CONFIG_SENSOR_DETECTION_MIN_HEIGHT && _handles[i].lastHeight_cm < CONFIG_SENSOR_DETECTION_MIN_HEIGHT) {
					ESP_LOGI(TAG, "Crossing detected on sensor %d, height=%dcm\n", i, objectHeight_cm);
					_handles[i].count++;
				}

				_handles[i].lastHeight_cm = objectHeight_cm;
			}
		}

	}
}


uint16_t crossing_getCount(bool reset) {
	uint16_t totalCount = 0;

	for (int i=0; i<_handlesLength; i++) {
		totalCount += _handles[i].count;
		if (reset) { // Reset count if asked
			_handles[i].count = 0;
		}
	}

	return totalCount;
}
