#include "network.h"

#include <stddef.h>
#include <stdio.h>
#include "esp_err.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "esp_wifi.h"
#include "esp_event.h"
#include "mqtt_client.h"

#define TAG	"Network"

static enum NetworkState {
	NSTATE_STOPPED = 0, // Not started or stopped
	NSTATE_WIFI_CONNECTING, // Started and waiting for WiFi connection to establish
	NSTATE_WIFI_CONNECTED, // Connectd to WiFi and got an IP address, waiting to start Mqtt connection
	NSTATE_MQTT_CONNECTING, // Waiting for Mqtt connection to establish
	NSTATE_MQTT_CONNECTED, // Everything is connected
	NSTATE_DISCONNECTING, // Trying to disconnect, initiated by network_stop()
} _state;
static esp_mqtt_client_handle_t _mqttClient;

static void _ip_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
static void _mqtt_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);

void network_init() {
	// Register event to stay informed of connection status
	esp_event_handler_instance_register(IP_EVENT, 	IP_EVENT_STA_GOT_IP, 	_ip_event_handler, NULL, NULL);
	esp_event_handler_instance_register(IP_EVENT, 	IP_EVENT_STA_LOST_IP,	_ip_event_handler, NULL, NULL);

	// Configure WiFi connection
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
	ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
	ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
	wifi_config_t sta_config = {
		.sta = {
			.ssid = CONFIG_WIFI_SSID,
			.password = CONFIG_WIFI_PASSWORD,
			.bssid_set = false
		}
	};
	ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &sta_config) );

	// Configure Mqtt connection
	esp_mqtt_client_config_t mqtt_cfg = {
		.uri = CONFIG_BROKER_URI,
		.client_id = CONFIG_DEVICE_ID,
		.username = "device-hmac",
		.password = CONFIG_DEVICE_KEY,
	};
	_mqttClient = esp_mqtt_client_init(&mqtt_cfg);
	esp_mqtt_client_register_event(_mqttClient, MQTT_EVENT_CONNECTED, 		_mqtt_event_handler, NULL);
	esp_mqtt_client_register_event(_mqttClient, MQTT_EVENT_DISCONNECTED, 	_mqtt_event_handler, NULL);

	// Init state
	_state = NSTATE_STOPPED;
}

void network_start() {
	// Actually start Wifi and connect to AP
	ESP_ERROR_CHECK( esp_wifi_start() );
	ESP_ERROR_CHECK( esp_wifi_connect() );
	_state = NSTATE_WIFI_CONNECTING;
}

void network_stop() {
	switch (_state) {
		case NSTATE_WIFI_CONNECTING:
			ESP_ERROR_CHECK( esp_wifi_stop() ); // Abort connection and stop WiFi
			_state = NSTATE_STOPPED;
			break;

		case NSTATE_WIFI_CONNECTED:
			ESP_ERROR_CHECK( esp_wifi_disconnect() ); // Disconnect and wait for it
			_state = NSTATE_DISCONNECTING;
			break;

		case NSTATE_MQTT_CONNECTING:
		case NSTATE_MQTT_CONNECTED:
			ESP_ERROR_CHECK( esp_mqtt_client_stop(_mqttClient) ); // Stop Mqtt client and wait for it
			_state = NSTATE_DISCONNECTING;
			break;

		default: // Do nothing, should not even be called
			break;
	}
}

bool network_isConnected() {
	return (_state == NSTATE_MQTT_CONNECTED);
}

void network_run() {
	switch (_state) {
		case NSTATE_WIFI_CONNECTED:
			ESP_ERROR_CHECK( esp_mqtt_client_start(_mqttClient) );
			_state = NSTATE_MQTT_CONNECTING;
			break;

		default:
			break;
	}
}

bool network_transmit(uint16_t crossingNumber) {
	if (_state != NSTATE_MQTT_CONNECTED) {
		return false;
	}

	// Prepare JSON formatted payload data
	char jsonPayload[100];
	sprintf(jsonPayload, "{ \"type\": \"measure\", \"value\": { \"crossing\": %d } }", crossingNumber);
	ESP_LOGI(TAG, "Enqueuing JSON payload to MQTT : [%s]\n", jsonPayload);

	// Enqueue payload in the Mqtt stack (don't use publish as it cause thread blocking during the transmission)
	int res = esp_mqtt_client_enqueue(_mqttClient, "iot/dev/"CONFIG_DEVICE_ID"/data", jsonPayload, 0, 2, 0, true);
	ESP_LOGI(TAG, "Payload enqueued with id %d", res);
	if (res == -1) {
		return false;
	}

	return true;
}

//------------------------------//
// 		Internal functions		//
//------------------------------//

static void _ip_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
	// Handle all WiFi/IP events
	if (event_base == IP_EVENT) {
		tcpip_adapter_ip_info_t ip;

		switch (event_id) {
			case IP_EVENT_STA_GOT_IP:
				ESP_ERROR_CHECK( tcpip_adapter_get_ip_info(ESP_IF_WIFI_STA, &ip) );
				ESP_LOGI(TAG, "GOT IP : "IPSTR"\n", IP2STR(&ip.ip));
				// Go to next state
				_state = NSTATE_WIFI_CONNECTED;
				break;

			case IP_EVENT_STA_LOST_IP:
				ESP_LOGI(TAG, "LOST IP\n");

				if (_state == NSTATE_DISCONNECTING) { // ok to get disconnected
					ESP_ERROR_CHECK( esp_wifi_stop() );
					_state = NSTATE_STOPPED;

				} else { // Not so ok about loosing connection
					ESP_ERROR_CHECK( esp_wifi_connect() ); // Try to reconnect
					_state = NSTATE_WIFI_CONNECTING;
				}
				break;
		}
	}
}

static void _mqtt_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
    switch ((esp_mqtt_event_id_t) event_id) {
		case MQTT_EVENT_CONNECTED:
			ESP_LOGI(TAG, "MQTT CONNECTED");
			_state = NSTATE_MQTT_CONNECTED;
			break;

		case MQTT_EVENT_DISCONNECTED:
			ESP_LOGI(TAG, "MQTT DISCONNECTED");
			if (_state == NSTATE_DISCONNECTING) { // ok to get disconnected
				// Continue disconnection
				ESP_ERROR_CHECK( esp_wifi_disconnect() );

			} else { // Not so ok about loosing connection
				ESP_ERROR_CHECK(  esp_mqtt_client_reconnect(_mqttClient) ); // Trying to reconnect
			}
			break;

		default:
			break;
		}
}
