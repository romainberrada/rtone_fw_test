#ifndef MAIN_VL53L1_H_
#define MAIN_VL53L1_H_

#include <stdint.h>
#include <stdbool.h>

struct VL53L1_Handle {
	uint8_t i2cAddress;
	int16_t lastRange;
};

/**
  * @brief Prepare I2C peripheral for VL53L1, Init ToF stack
  *
  * @param[in,out] handle Handle structure for this sensor instance
  *
  * @return true if initialization went ok
  * 		false if an error occurred during init
  */
bool vl53l1_init(struct VL53L1_Handle* handle, uint8_t i2cAddress);

/**
  * @brief Do required work to ensure correct sensing
  * This function should be called regularly from the main to ensure good operations
  *
  * @param[in,out] handle Handle structure for this sensor instance
  *
  * @return true if a new range is available
  * 		false if no new event occurred
  */
bool vl53l1_run(struct VL53L1_Handle* handle);

/**
  * @brief Get last measured range in mm
  *
  * @param[in] handle Handle structure for this sensor instance
  *
  * @return Actual range in mm
  * 		-1 if never ranged
  */
int16_t vl53l1_getLastRange(struct VL53L1_Handle* handle);

#endif /* MAIN_VL53L1_H_ */
