#ifndef MAIN_CROSSING_H_
#define MAIN_CROSSING_H_

#include <stdbool.h>
#include <stdint.h>

/**
  * @brief Start and initialize the underlying VL53L1 arrays. Prepare the signal processing and distance decision making
  *
  * @param[in] i2cArray A list of I2C addresses for the full sensor array
  * @param[in] floorDistances A list of distances between the sensor and the floor (in cm)
  * @param[in] length Length of the i2c_array
  */
void crossing_init(uint8_t* i2cArray, uint16_t* floorDistances, int length);

/**
  * @brief Do required work to ensure correct sensing
  * This function should be called regularly from the main to ensure good operations
  */
void crossing_run();

/**
  * @brief Get the number of crossing register by the sensor array
  *
  * @param reset Keep (false) or Reset (true) the current count of crossing
  *
  * @return Number of crossings across the whole sensor array
  */
uint16_t crossing_getCount(bool reset);

#endif /* MAIN_CROSSING_H_ */
