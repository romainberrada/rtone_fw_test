#include "vl53l1.h"
#include "esp_random.h"
#include "esp_log.h"

#define TAG "VL53L1_Mock"

bool vl53l1_init(struct VL53L1_Handle* handle, uint8_t i2cAddress) {
	handle->i2cAddress = i2cAddress;
	handle->lastRange = -1;
	return true;
}

bool vl53l1_run(struct VL53L1_Handle* handle) {
	uint32_t random = esp_random(); // Should get actual random number as WiFi is enabled

	random &= (128-1); // Only retrieve 9 bits [0-127]

	// People crossing on a 0
	// Odds are 1/127
	// If called every 100ms, should statistically get a crossing every ~12s
	if (!random) {
		handle->lastRange = 650; // Oh boy we got a 1.85m dude(tte)
	} else {
		ESP_LOGD(TAG, "Random crossing mock\n");
		handle->lastRange = 2500; // 2.5m from ceiling to ground
	}

	return true; // Always getting a new range on each call
}

int16_t vl53l1_getLastRange(struct VL53L1_Handle* handle) {
	return handle->lastRange;
}
