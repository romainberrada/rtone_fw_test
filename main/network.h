#ifndef MAIN_NETWORK_H_
#define MAIN_NETWORK_H_

#include <stdbool.h>
#include <stdint.h>

/**
  * @brief Prepare and configure underlying data structure to connecto to WiFi network and MQTT broker.
  */
void network_init();

/**
  * @brief Start radio network peripherals, connect to AP and to MQTT broker
  */
void network_start();

/**
  * @brief Disconnect from MQTT and WiFi access point, and disable peripherals
  */
void network_start();

/**
  * @brief Check if connection is established with MQTT broken
  *
  * @return true if ready to publish
  * 		false if not connected (either internet or mqtt broker)
  */
bool network_isConnected();

/**
  * @brief Do required work to ensure stable and working connection
  * This function should be called regularly from the main to ensure good operations
  */
void network_run();

/**
  * @brief Push data to Cloud
  * @param[in]  crossingNumber   Number of people crossing since last update
  *
  * @return true if transmission was initiated
  * 		false if not connected or error occurred before transmitting
  */
bool network_transmit(uint16_t crossingNumber);

#endif /* MAIN_NETWORK_H_ */
