# PoC : Corridor Count
======================

## Description

This project is aimed at counting people crossing (in corridors for example) and send a count value to Cloud regularly.

Project is built on ESP32 target and created with Espressif IDE

It is based on the default ESP32 project template (so probably not everything is optimized).

## State of the project

Version : v1.0

As of writing (30/03), project is built with first draft of Cloud communication and Crossing algorithm.

Actual VL53L1 sensor is mocked and is generating 65cm of distance randomly (65cm simulates a 185cm people with sensor attach at 250cm from floor). 

## Architecture

As described in the Theorical part document, the code is split in 4 modules :

* Hardware sensor driver. It manages the underlying connection and protocol/registers of the sensor
* Software sensor drvier. It manages the Hardware driver and compute crossing count based on it
* Network. It handles WiFi and MQTT connection to provide a easy-to-use start/stop/transmit interface
* Main. Simple glue to init/configure everything and regularly transmit data to cloud

Interactions are described as below :

Main -> Network

Main -> Software driver -> Hardware driver

## Configure project

Based on KConfig menu generation, the project creates 4 menus :

* WiFi Configuration. Fill your SSID/Password
* MQTT Configuration. Fill Broker URI, Device Id and Device Key
* Crossing Configuration. Maximum array size for sensor array (10 should be plenty enough). Fill Pepople height (in centimeters) to register a crossing
* App Configuration. Fill the delay (in minutes) bewteen two consecutive data transmission to cloud

## Build & Run

Espressif IDE does all that part based on the project CMake and the ESP-IDF install.
